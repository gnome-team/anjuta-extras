#!/bin/sh
echo -n "Description="
find . -iname \*.plugin.in | sort | while read f; do cat $f | awk -f debian/desktop2description.awk | sed 's/.*/ &\$\{Newline\}/' | tr -d '\n';  done
echo
